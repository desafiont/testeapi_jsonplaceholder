@JSONPlaceHolder
Feature: Validar fluxos e dados da API JSONPlaceholder

  @ValidarStatusCode200
  Scenario: Validar Status Code 200
    Given que acessei a API
    When acessar Posts
    Then valido o acesso

  @ValidarStatusCode404
  Scenario: Validar Status Code 404
    Given que acessei JsonPlace Holder
    When acessar endereco incorreto
    Then valido invalido

  @ValidarGetID1
  Scenario: Validar pesquisa por ID1
    Given que acessei o endereco da API
    When acessar ID no Posts
    Then valido os dados no Posts

  @ValidarStatusCode500
  Scenario: Validar Status Code 500
    Given que acesso esta incompleto
    When acessar endereco
    Then valido status 500


  @ValidarPost
  Scenario: Validar inclusao de Posts
    Given que acessei
    When acessar tela inicial e incluir dados
    Then valido a inclusao

  @ValidarPut
  Scenario: Validar atualizacao
    Given que utilizei URL para Update
    When enviar dados para atualizacao
    Then validar atualizacao

  @ValidarDelete
  Scenario: Validar exclusao de Post
    Given que utilizei acesso para exclusao de Post
    When acessar tela inicial excluir dado inserido no Post
    Then valido a exclusao