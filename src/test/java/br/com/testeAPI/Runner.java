package br.com.testeAPI;

import br.com.testeAPI.api.BaseTest;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/Feature",
    tags = "@ValidarPut", plugin = {"json:reports/cucumber.json"})
public class Runner extends BaseTest {
}