package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarSucesso {

    public RequestSpecification request;
    public Response response;

    @Given("que acessei a API")
    public void que_acessei_a_API() throws Throwable {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("acessar Posts")
    public void acessar_Posts() throws Throwable {
        response = request.when().get("/posts").prettyPeek();
    }

    @Then("valido o acesso")
    public void valido_o_acesso() throws Throwable {
        Assert.assertTrue(response.statusCode() == 200);
    }
}
