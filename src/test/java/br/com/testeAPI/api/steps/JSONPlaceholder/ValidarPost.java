package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarPost {

    public RequestSpecification request;
    public Response response;

    @Given("que acessei")
    public void que_acessei() throws Throwable {
        request  = RestAssured.given().contentType("application/json; charset=UTF-8")
                .body("{\n" +
                        "      \"title\": \"teste\",\n" +
                        "      \"body\": \"teste1\",\n" +
                        "      \"userId\": 27\n" +
                        "    }");
    }

    @When("acessar tela inicial e incluir dados")
    public void acessar_tela_inicial_e_incluir_dados() throws Throwable {
        response = request.when().post("/posts").prettyPeek();
    }

    @Then("valido a inclusao")
    public void valido_a_inclusao() throws Throwable {
        Assert.assertEquals(response.jsonPath().getInt("userId"), 27);
        Assert.assertEquals(response.jsonPath().get("title"), "teste");

    }

}
