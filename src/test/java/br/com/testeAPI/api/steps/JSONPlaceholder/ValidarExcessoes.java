package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarExcessoes {

    public RequestSpecification request;
    public Response response;

    @Given("que acessei o endereco da API")
    public void que_acessei_o_endereco_da_API() throws Throwable {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("acessar ID no Posts")
    public void acessar_ID_no_Posts() throws Throwable {
        response = request.when().get("/posts/1").prettyPeek();
    }

    @Then("valido os dados no Posts")
    public void valido_os_dados_no_Posts() throws Throwable {
        Assert.assertEquals(response.jsonPath().getInt("id"), 1);
        Assert.assertEquals(response.jsonPath().getInt("userId"), 1);
        Assert.assertEquals(response.jsonPath().get("title"), "sunt aut facere repellat provident occaecati excepturi optio reprehenderit");

    }

    @Given("que acesso esta incompleto")
    public void que_acesso_esta_incompleto() throws Throwable {
        request  = RestAssured.given().contentType("application/json; charset=UTF-8")
                .body("{title: 'foo', body: 'bar', userId: 1}");
    }

    @When("acessar endereco")
    public void acessar_endereco() throws Throwable {
        response = request.when().post("/posts").prettyPeek();
    }

    @Then("valido status 500")
    public void valido_status() throws Throwable {
        Assert.assertTrue(response.statusCode() == 500);
    }

}
