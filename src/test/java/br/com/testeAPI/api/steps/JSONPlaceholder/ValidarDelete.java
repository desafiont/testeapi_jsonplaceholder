package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarDelete {

    public RequestSpecification request;
    public Response response;

    @Given("que utilizei acesso para exclusao de Post")
    public void que_utilizei_acesso_para_exclusao_de_Post() throws Throwable {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("acessar tela inicial excluir dado inserido no Post")
    public void acessar_tela_inicial_excluir_dado_inserido_no_Post() throws Throwable {
        response = request.when().delete("/posts/1").prettyPeek();
    }

    @Then("valido a exclusao")
    public void valido_a_exclusao() throws Throwable {
        Assert.assertTrue(response.statusCode() == 200);
    }
}
