package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarNotFound {

    public RequestSpecification request;
    public Response response;

    @Given("^que acessei JsonPlace Holder$")
    public void que_acessei_JsonPlace_Holder() throws Throwable {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("acessar endereco incorreto")
    public void acessar_endereco_incorreto() throws Throwable {
        response = request.when().get("/post").prettyPeek();
    }

    @Then("valido invalido")
    public void valido_invalido() throws Throwable {
        Assert.assertTrue(response.statusCode() == 404);
    }
}
