package br.com.testeAPI.api.steps.JSONPlaceholder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarPut {

    public RequestSpecification request;
    public Response response;

    @Given("que utilizei URL para Update")
    public void que_utilizei_URL_para_Update() throws Throwable {
        request  = RestAssured.given().contentType("application/json; charset=UTF-8")
                .body("{\n" +
                        "      id: 1,\n" +
                        "      title: 'foo',\n" +
                        "      body: 'bar',\n" +
                        "      userId: 1\n" +
                        "    }");
    }

    @When("enviar dados para atualizacao")
    public void enviar_dados_para_atualizacao() throws Throwable {
        response = request.when().put("/posts/1").prettyPeek();
    }

    @Then("validar atualizacao")
    public void validar_atualizacao() throws Throwable {
        //Assert.assertTrue(response.statusCode() == 200);
    }


}
