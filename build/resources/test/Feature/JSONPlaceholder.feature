@JSONPlaceHolder
Feature: Validar fluxos e dados da API JSONPlaceholder

  @ValidarStatusCode200
  Scenario: Validar Status Code 200
    Given que acessei a API
    When acessar Posts
    Then valido o acesso

  @ValidarStatusCode404
  Scenario: Validar Status Code 404
    Given que acessei JsonPlace Holder
    When acessar endereco incorreto
    Then valido invalido